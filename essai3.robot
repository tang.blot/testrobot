*** Settings ***
Resource	squash_resources2.resource
*** Variables ***
&{Jdd1}    facility=Tokyo    HealthcareProgram=radio_program_medicare    verifHealthcareProgram=Medicare
&{Jdd2}    facility=Hongkong   HealthcareProgram=radio_program_medicaid    verifHealthcareProgram=Medicaid
&{Jdd3}    facility=Seoul    HealthcareProgram=radio_program_none   verifHealthcareProgram=None
*** Keywords ***
Test Setup
	Open Browser     https://katalon-demo-cura.herokuapp.com     firefox
    Maximize Browser Window

Test Teardown
	Sleep     5
    Close Browser


*** Test Cases ***
Test3-1
    [Tags]    Test1
	[Setup]	Test Setup

	Given je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires    ${Jdd1.facility}
	And Je choisi HealthcareProgram    ${Jdd1.HealthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram est affiché     ${Jdd1.verifHealthcareProgram}       ${Jdd1.facility}


	[Teardown]	Test Teardown

Test3-2
    [Tags]    Test2     Test1
	[Setup]	Test Setup

	Given je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires    ${Jdd2.facility}
	And Je choisi HealthcareProgram    ${Jdd2.HealthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram est affiché     ${Jdd2.verifHealthcareProgram}       ${Jdd2.facility}


	[Teardown]	Test Teardown

Test3-3
    [Tags]    Test1
	[Setup]	Test Setup

	Given je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires    ${Jdd3.facility}
	And Je choisi HealthcareProgram    ${Jdd3.HealthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram est affiché     ${Jdd3.verifHealthcareProgram}       ${Jdd3.facility}


	[Teardown]	Test Teardown

